\documentclass[twoside, a4paper, 12pt, openright, fullpage]{article}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage{gensymb}
\usepackage{eurosym}
\usepackage{geometry}
\usepackage{siunitx}
\usepackage{textcomp} 
\usepackage{float}
\usepackage{amsmath}
\usepackage{empheq}
\usepackage{hyperref}
\geometry{hmargin=2.5cm,vmargin=1.5cm}




\title{Report ML Project}
\author{Mélissa Tamine}
\date{\today}

\begin{document}

\begin{titlepage}
\addtolength{\hoffset}{-0.3cm}
	\centering
	\includegraphics[width=1.0\textwidth]{IMT logo.png}\par\vspace{1cm}
	{\scshape\LARGE Mathematical and computational engineering \par}
	\vspace{1cm}
	{\scshape\Large Report \par}
	\vspace{1.5cm}
	{\huge\bfseries Machine Learning Project \par}
	\vspace{2cm}
	{\Large\itshape Pierre \textsc{Charreaux}, \Large\itshape Jean \textsc{Plumail}, \Large\itshape Lucas \textsc{Rakotoarivony} \Large\itshape Mélissa \textsc{Tamine} \par}
	\vfill
	supervised by \par
	\textsc{Dupraz Elsa}

	\vfill
	{\large \today \par}
	\newpage
\end{titlepage}

\tableofcontents
\newpage

\section{Presentation of the project}
The objective of the project is to \textbf{collaboratively} implement and apply a Machine Learning model onto two different datasets.\\ \\
We chose to address a \textbf{binary classification problem} and apply it onto the following datasets : the \href{https://archive.ics.uci.edu/ml/datasets/banknote+authentication}{\textit{"Banknote Authentication dataset"}} and the \href{https://www.kaggle.com/mansoordaku/ckdisease}{\textit{"Chronic Kidney Disease dataset"}}.
\subsection{Machine Learning Workflow}
The \textbf{Machine Learning Workflow} that we need to implement and apply to both datasets is the following one :
\begin{enumerate}
    \item Import the dataset
    \item Clean the data and perform pre-processing :
    \begin{itemize}
        \item Replace missing values by average or median values
        \item Center and normalize the data
    \end{itemize}
    \item Split the dataset
    \begin{itemize}
        \item Split between training set and test set
        \item Split the training set for cross-validation
    \end{itemize}
    \item Train the model (including feature selection)
    \item Validate the model
\end{enumerate}

\subsection{The use of Git to code collaboratively}
\textbf{Git} is a distributed version-control system for tracking changes in source code during software development. It is designed for coordinating work among programmers, but it can be used to track changes in any set of files. Its goals include speed, data integrity, and support for distributed, non-linear workflows. \\ \\
This software therefore has two main purposes :
\begin{enumerate}
    \item to follow the evolution of a source code, to retain the changes made on each file and be able to go back in case of a problem;
    \item to work together, without risking stepping on each other’s toes. If two people edit the same file at the same time, their changes must be able to be merged without loss of information.
\end{enumerate}
\begin{figure}[H]
    \centering
    \includegraphics[width=0.3\textwidth]{Git.png}
    \caption{Mapping of a distributed version management software with a server such as Git}
    \label{fig:my_label}
\end{figure}
One of the particularities of Git is the existence of collaborative Git-based websites like GitHub, or GitLab. As part of our project, \textbf{we chose to work with the GitLab platform of IMT Atlantique}. 
\subsection{Some reminders on binary classification}
In machine learning, classification refers to a predictive modeling problem where a class label is predicted for a given example of input data and \textbf{binary classification} refers to those classification tasks that have two class labels. Examples include email spam detection (spam or not) for example. \\ \\
Typically, binary classification tasks involve one class that is the normal state and another class that is the abnormal state. For example “not spam” is the normal state and “spam” is the abnormal state. The class for the normal state is assigned the class label 0 and the class with the abnormal state is assigned the class label 1. \\ \\
It is common to model a binary classification task with a model that predicts a Bernoulli probability distribution for each example. The Bernoulli distribution is a discrete probability distribution that covers a case where an event will have a binary outcome as either a 0 or 1. For classification, this means that the model predicts a probability of an example belonging to class 1, or the abnormal state.
\\ \\
Popular algorithms that can be used for binary classification include :
\begin{itemize}
    \item Logistic Regression
    \item k-Nearest Neighbors
    \item Support Vector Machine
    \item Decision Trees
\end{itemize}
%\begin{figure}[H]
    %\centering
    %\includegraphics[width=0.5\textwidth]{SVM_example.png}
    %\caption{An example of contour plot of SVM decision values for a binary classification problem}
    %\label{fig:my_label}
%\end{figure}
\section{Presentation of the datasets}
\subsection{The Banknote Authentication dataset}
The Banknote Authentication dataset is a well-known set of benchmark data for binary classification problems. \\ \\
This dataset set consists of 1372 items. \\ \\
The goal of applying binary classification on this dataset is to predict whether a banknote (Euro or dollar bill for example) is authentic/real/genuine or a forgery/fake based on four predictor values. The four predictors are numbers that measure characteristics of digital images of each banknote. \\ \\ 
Each line of data represents a banknote. The first four values on each line are the predictor values (which are continuous) : the variance of Wavelet Transformed image, the skewness of Wavelet Transformed image, the curtosis of Wavelet Transformed image, and the entropy of image. The last value on each line is the class to predict, 0 or 1 (0 for real/genuine and 1 for forgery). 
\subsection{The Chronic Kidney Disease dataset}
The chronic kidney disease (CKD) dataset is a well-known set of benchmark data for binary classification problems. \\ \\
The data are blood tests and other measures from patients with and without CKD. There are 400 rows, one per patient; these are patients seen over a period of about two months at some point before July 2015, in a hospital in Tamil Nadu, India; maybe Apollo Reach Karaikudi.
\\ \\
Of the 400 rows, 250 correspond to patients with CKD and the remaining 150 rows correspond to patients without CKD. This information is in the “Class” column of the dataset. \\ \\
Here is a description of each column : 
\begin{enumerate}
    \item Age : in years
    \item Blood Pressure: : in mm/Hg
    \item Specific Gravity : one of (1.005,1.010,1.015,1.020,1.025)
    \item Albumin : one of (0,1,2,3,4,5)
    \item Sugar : one of (0,1,2,3,4,5)
    \item Red Blood Cells: one of (“normal”, “abnormal”)
    \item Pus Cell : one of (“normal”, “abnormal”)
    \item Pus Cell clumps : one of (“present”, “notpresent”)
    \item Bacteria : one of (“present”, “notpresent”)
    \item Blood Glucose Random : in mgs/dl
    \item Blood Urea : in mgs/dl
    \item Serum Creatinine : in mgs/dl
    \item Sodium : in mEq/L
    \item Potassium : in mEq/L
    \item Hemoglobin : in gms
    \item Packed Cell Volume : (volume percentage)
    \item White Blood Cell Count : in cells/cumm
    \item Red Blood Cell Count : in millions/cmm
    \item Hypertension : one of (“yes”, “no”)
    \item Diabetes Mellitus : one of (“yes”, “no”)
    \item Coronary Artery Disease : one of (“yes”, “no”)
    \item Appetite : one of (“good”, “poor”)
    \item Pedal Edema : one of (“yes”, “no”)
    \item Anemia : one of (“yes”, “no”)
    \item Class : one of (“ckd”, “notckd”)
\end{enumerate}
\section{Methods used during the project}
In this project, we consider two problems of binary classifications (we seek to classify two sets of data). To solve these problems we chose to use two methods :
\begin{itemize}
    \item Logistic regression
    \item Support Vector Machine
\end{itemize}
\subsection{Logistic regression}
Some regression algorithms can be used for classification as well (and vice versa). It is the case with Logistic Regression which is commonly used to estimate the probability that an instance belongs to a particular class. If the estimated probability is greater than 50\%, then the model predicts that the instance belongs to that class (called the positive class, labeled “1”), or else it predicts that it does not (i.e., it belongs to the negative class, labeled “0”). This makes it a binary classifier. \\ \\
So how does it work ? Just like a Linear Regression model, a Logistic Regression model computes a weighted sum of the input features (plus a bias term), but instead of outputting the result directly like the Linear Regression model does, it outputs the logistic of this result : $\widehat{p} = h_{\theta}(x) = \sigma(x^T\theta)$
\\ \\
The logistic, noted $\sigma$, is a sigmoid function that outputs a number between 0 and 1. It is defined as followed : $\sigma(t)=\frac{1}{1+\exp(-t)}$. \\ \\
Once the Logistic Regression model has estimated the probability $\widehat{p} = h_{\theta}(x)$ that an instance x belongs to the positive class, it can make its prediction $\widehat{y}$ easily : 
$$
\widehat{y} = \left\{
    \begin{array}{ll}
        0 & \mbox{if } \widehat{p} \leq 0.5 \\
        1 & \mbox{else}
    \end{array}
\right.
$$
\subsection{Support Vector Machine}
A Support Vector Machine (SVM) is a very powerful and versatile Machine Learning model, capable of performing linear or nonlinear classification, regression, and even outlier detection. It is one of the most popular models in Machine Learning. \\ \\ SVMs are particularly well suited for classification of complex but small or medium-sized datasets which is our problem case. \\ \\
How does a SVM work ? At first approximation what linear SVMs do is to find a separating line (or hyperplane) between data of two classes. SVM is an algorithm that takes the data as an input and outputs a line that separates those classes if possible. According to the SVM algorithm we find the points closest to the line from both the classes.These points are called \textbf{support vectors}. Now, we compute the distance between the line and the support vectors. This distance is called the margin. Our goal is to maximize the margin. \textbf{The hyperplane for which the margin is maximum is the optimal hyperplane}. \\ \\
Although linear SVM classifiers are efficient and work surprisingly well in many cases, many datasets are not even close to being linearly separable. One approach to handling nonlinear datasets is to add more features, such as polynomial features for example, or apply an almost miraculous mathematical technique called the \textbf{kernel trick}. Some commons kernels :
\begin{itemize}
    \item Linear : $K(a,b) = a^T b$
    \item Polynomial : $K(a,b) = (\gamma a^T b+r)^d$
    \item Gaussian RBF : $K(a,b) = \exp(−\gamma \mid \mid a-b \mid \mid^2)∥$
    \item Sigmoid : $K(a,b) = \text{tanh}(γa^T b+r)$
\end{itemize}
\section{The main development steps of the project}
\begin{enumerate}
    \item \textbf{Import both datasets} : We started by importing the two datasets described above and contained respectively in a text file and a .csv file. We used pandas to convert them into Dataframe objects. By using the .head() function, we were able to visualize some of these datasets in order to better understand the data clean-up step.
    \item \textbf{Clean the datasets} : Subsequently, we implemented different functions all called in a global function clean\_data()  allowing to suppress columns of the dataset containg too much NaN, to encode nominal data into numerical data using integers and to replace missing values by average values. We apply the clean\_data() function to both of the datasets. However, for the Banknote Authentication dataset, it does nothing since this dataset is already cleaned.
    \item \textbf{Split the data} : We split the data between a training set and a testing set (80\% and 20\% of the dataset respectively), shuffling it beforehand. \newline Then, we standardize our data using the mean and the standard deviation of our training set.
    \item \textbf{Train the model (including feature selection)} : Then, we select relevant features and train a model, working on the training set. \newline Two features selection methods are tested:
    \begin{enumerate}
        \item Feature correlation : we only keep "useful" features whose correlations with class labels are greater than a certain value (we chose 0.01).
        \item PCA or Principal Component Analysis : we apply a PCA method to our dataset using the default scikit-learn's implementation.
    \end{enumerate}
    Then, we use the two classification methods previously described :
    \begin{enumerate}
        \item Logistic Regression : we used the default scikit-learn's implementation.
        \item Support Vector Machine : with a Gaussian RBF kernel and cost $C$ of $1$ using scikit-learn's SVC function.
    \end{enumerate}
    \item \textbf{Validate the model}. To evaluate the performance of our models, we compute the accuracy scores : the proportion of correct guesses over the test set.
\end{enumerate}
\section{The results of our study}
\section{Good programming practices we used}
In machine learning, the ability to reproduce someone's work is important. It allows everyone to see how the model is built and if there is any mistake affecting conclusions. \newline To improve our programming workflow, we focused on 3 main points.
\begin{enumerate}
    \item \textbf{Automation of data processing} \newline Before playing with machine learning models, datasets have to come through several processing steps. These steps should not be made by hand (with Excel for example): it would make the process unclear and not reproducible. That's why we only modified our datasets with coded functions that we described in Section 4.
    \item \textbf{Using version control} \newline We used Git during our project to share our code and the datasets. Having only one version of the work on each of our computers helped us in the building proces. Everyone could easily reproduce someone's work and check if the results were correct. We were also able to come back to previous versions when needed.
    \item \textbf{Sharing our environment} \newline We also tried to work on the same Python environment during this project. Having different versions of Python modules could lead to different outputs. We created a conda environment with the packages we use, so that anyone can reproduce the work on his machine.
\end{enumerate}

\end{document}