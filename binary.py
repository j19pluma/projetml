import numpy as np
import random as rd
import matplotlib.pyplot as plt
import pandas as pd
from sklearn.preprocessing import StandardScaler
from sklearn.model_selection import train_test_split
from sklearn import svm
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from io import StringIO
from sklearn.decomposition import PCA
import copy
from sklearn.metrics import confusion_matrix
import itertools

"""1. Import the dataset"""

def import_data(path, header=None, names=None, index_col=None, na_values=None, trash=[]): # @Author : PLUMAIL Jean
    classic_delimiters = ["\t", ",", ";"]
    with open(path, "r") as f:
        content = f.read()
        for t in trash:
            content = content.replace(t, "")
    df = pd.read_csv(StringIO(content), delimiter=",", header=header, names=names, index_col=index_col, na_values=na_values)
    return df


"""2. Clean the data and perform pre-processing"""

def suppress_column(data) : # @Author : TAMINE Mélissa

    """
    Function allowing to suppress columns of the dataset containg too much NaN (>1/5 of the dataset)

    IN : 
    ----------
    data : TYPE pandas DataFrame

    OUT :
    -------
    new_data : TYPE pandas DataFrame with less columns

    """
    (l,c) = data.shape
    new_data = data.copy()
    A = new_data.isnull().sum() 
    for column_name in A.keys() :
        if A[column_name]>(l/5) :
            new_data.drop(column_name, axis=1,inplace=True)
    return new_data

#

def encode(data) : # @Author : TAMINE Mélissa and PLUMAIL Jean
    """
    Function allowing to encode nominal data into numerical data using integers
    a dictionnary T is returned to keep track of which name corresponds to each integer

    IN : 
    ----------
    data : TYPE pandas DataFrame

    OUT :
    -------
    data_copy : TYPE pandas DataFrame
    T : TYPE dict of dict ( feature name -> (class index -> class name) )

    """

    data_copy=data.copy()
    T = {}
    for col in data_copy.columns:
        data_col = data_copy[col]
        if data_col.dtypes == 'object':
            mapper = {c: i for i, c in enumerate(data_col.unique())}
            data_copy[col] = data_col.map(mapper)
            T[col] = {i: c for c, i in mapper.items()}
    return (data_copy, T)

def replace_na(data): # @Author : PLUMAIL Jean
    """
    Replace N/A values by the mean

    IN : 
    ----------
    data : TYPE pandas DataFrame

    OUT :
    -------
    data_copy : TYPE pandas DataFrame

    """
    data_copy = data.copy()
    for col in data_copy.columns:
        col_na = data[col].isna() # rows that are incomplete for this column
        if data_copy[col].dtypes != "object": # if we have numerical data
            data_copy.loc[col_na, col] = data_copy[col][~col_na].mean()
        else: # we have nominal data, replace na values by the most common value in the column
            data_copy.loc[col_na, col] = data_copy[col].value_counts().idxmax()
    return data_copy

def clean_data(data): # @Author : PLUMAIL Jean
    """
    Does all the cleaning

    IN : 
    ----------
    data : TYPE pandas DataFrame

    OUT :
    -------
    data_copy : TYPE pandas DataFrame
    labels : TYPE list
    T : TYPE dict of dict ( feature name -> (class index -> class name) )

    """
    
    data_copy = data.copy()
    data_copy = suppress_column(data_copy)
    data_copy = replace_na(data_copy)
    data_copy, T = encode(data_copy)
    labels = data_copy["classification"]
    data_copy.drop(columns="classification", inplace=True)
    return data_copy, labels, T


# =============================================================================
# Two cases:
# If 0<n_components<1, n_components indicating the ratio of variance you wish to preserve
# If n_components is in N*, n_components represents the number of features that we use 
# Return the features reduced and the PCA (if we use new data)
# For training the PCA we obviously use the train set
# =============================================================================

def PCA_reduced(X_train,X_test,n_components=0.95) :# @Author : RAKOTOARIVONY Lucas
    pca = PCA(n_components=n_components)
    X_train_reduced = pca.fit_transform(X_train)
    X_test_reduced = pca.transform(X_test)
    return X_train_reduced,X_test_reduced, pca


# =============================================================================
# Remove the features who are not correlated with the result (correlation between classification and the features<value)
# Return the new dataframe and the list of the useful_features
# =============================================================================

def correlation(df,label,value=0.01): # @Author : RAKOTOARIVONY Lucas
    new_df=copy.deepcopy(df)
    new_df["classification"]=label
    list_features=list(new_df)
    matrix = new_df.corr()
    useful_features=[]
    res= matrix["classification"].sort_values(ascending=False)
    for feature in list_features:
        if(abs(res[feature])<value):
            useful_features.append(feature)
    for useful_feature in useful_features:
        del new_df[useful_feature]
    del new_df["classification"]
    return new_df,useful_features

# =============================================================================
# Remove the features in liste (useful features)
# =============================================================================

def suppresion(df,liste): # @Author : RAKOTOARIVONY Lucas
    for l in liste:
        del df[l]
    return df

# =============================================================================
# Remove the features who are not correlated with the result (correlation between classification and the features<value) via a train set
# Return the new dataframe and the list of the useful_features
# We find the useful features according to the train set
# =============================================================================

def feature_correlation(X_train,y_train,X_test,name_col,value=0.01): # @Author : RAKOTOARIVONY Lucas
    df_train = pd.DataFrame(X_train, columns = name_col)
    labels = pd.DataFrame(y_train, columns = ["classification"])
    features,useful_feature = correlation(df_train,labels,value=value) #on étudie sur le dataset de train les features importantes
    X_train_reduced = features.to_numpy() #data
    
    df_test = pd.DataFrame(X_test, columns = name_col)
    df_test = suppresion(df_test,useful_feature) #on supprime les valeurs inutiles
    
    X_test_reduced = df_test.to_numpy()
    return X_train_reduced,X_test_reduced,useful_feature

"""3. Split the dataset"""

# =============================================================================
# We split the dataset with a ratio 0.8 train set and 0.2 test set and we apply a normalization via StandardScaler
# =============================================================================

def split(X,y,test_size=0.2) : # @Author : RAKOTOARIVONY Lucas
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)
    scaler = StandardScaler().fit(X_train)
    X_train=scaler.transform(X_train)
    X_test=scaler.transform(X_test)
    return X_train, X_test, y_train, y_test

"""4. Train the model"""

# =============================================================================
# Methode for training an SVM
# We can change the hyperparameters but by default we keep the classic hyperparameters
# =============================================================================

def training_SVM(X_train,y_train,C=1,kernel='rbf'): # @Author : RAKOTOARIVONY Lucas
    clf = svm.SVC(C=C,kernel=kernel)
    clf.fit(X_train,y_train)
    return clf ,clf.score(X_train,y_train)

# =============================================================================
# Methode for training an Logistic Regression
# =============================================================================

def training_logistic(X_train,y_train): # @Author : RAKOTOARIVONY Lucas
    clf = LogisticRegression()
    clf.fit(X_train,y_train)
    return clf, clf.score(X_train,y_train)

# =============================================================================
# Methode who return the validation score for a classifier
# =============================================================================


def validation(X_test,y_test,clf): # @Author : RAKOTOARIVONY Lucas
    y_pred=clf.predict(X_test)
    return accuracy_score(y_test, y_pred)

def plot_confusion_matrix(X_test,y_test,clf,T,title): # @Author : CHARREAUX Pierre, TAMINE Mélissa
    """
    Plot a confusion matrix out of the test set and a classifier

    IN : 
    ----------
    X_test : TYPE numpy array (the set of the features vector used to do the classification)
    
    y_test : TYPE numpy array (label of the test set)
    
    clf : classifier
    
    T : TYPE dict of dict ( feature name -> (class index -> class name) )
    
    title : TYPE String  (title of the matrix)

    OUT :
    -------
   
    """
    y_pred = clf.predict(X_test)
    cmap =plt.cm.Blues
    if T=={}:
        classes = ['true','false']
    else:
        classes = T['classification'].values()
    cm = confusion_matrix(y_test,y_pred,normalize='true')
    cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=45)
    plt.yticks(tick_marks, classes)

    fmt = '.2f'
    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j,0.8*(i+0.1), format(cm[i, j], fmt),
                 horizontalalignment="center",
                 verticalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('true label')
    plt.xlabel('predicted label')

"""5. Plot the data""" 

def is_discret(numbers_in,i,j): # @Author : CHARREAUX Pierre, TAMINE Mélissa
    """
    tells if one of the two coordiantes is considered as discreet (it has less than 11 values here)

    IN : 
    ----------
    numbers_in : TYPE list (numbers_in[i] is the number of values that coordinates i takes)
    
    i : TYPE int (index of one of the two coordinates)
    
    j : TYPE int (index of one of the coordinates)

    OUT :
    -------
    
    TYPE boolean  (whether or not one of the coordinates is discreet)
    
    TYPE int  (the index of the discreet coordiantes that was detected first, -1 if none are)
    """
    
    if (numbers_in[i]<=11):
        return True,i
    elif (numbers_in[j]<=11):
        return True,j
    else:
        return False,-1

def give_columns_numbers(features): # @Author : CHARREAUX Pierre, TAMINE Mélissa
    """
    returns the names of the columns and the number of values each column takes

    IN : 
    ----------
    features : TYPE Pandas data frame  (the first thing coming out from the clean data function)

    OUT :
    -------
    list_columns : TYPE list of String (the list of the names of the columns)
    
    numbers_in : TYPE list of int (the list of the number of different values each column takes)
    
    """
    
    list_columns =features.columns
    numbers_in = []
    for elt in list_columns:
        nb = 0
        seen = []
        for thing in features[elt]:
            if not(thing in seen):
                nb+=1
                seen.append(thing)
        numbers_in.append(nb)
    return list_columns, numbers_in
    
   
def plot_non_discret(nb_data,X,y,T,list_columns,i,j): # @Author : CHARREAUX Pierre
    """
    Plot the graph of the coordinates when none of them are discreet

    IN : 
    ----------
    nb_data : TYPE int (number of data)
    
    X: TYPE numpy array (the features vector used to do the classification)
    
    y : TYPE numpy array (label of the data)
        
    T : TYPE dict of dict ( feature name -> (class index -> class name) )
    
    list_columns : TYPE list of String (the names of the columns)
    
    i : TYPE int (index of the first feature used for the graph)
    
    j : TYPE int (index of the second feature used for the graph)

    OUT :
    -------
    """
    plot_X = []
    plot_Y = []
    X0 = [X[k,i] for k in range(nb_data) if y[k] == 0 ]
    Y0 = [X[k,j] for k in range(nb_data) if y[k] == 0 ]
    X1 = [X[k,i] for k in range(nb_data) if y[k] == 1 ]
    Y1 = [X[k,j] for k in range(nb_data) if y[k] == 1 ]
    if T == {}:
        plt.scatter(X0,Y0,c = 'green', label = 'True')
        plt.scatter(X1,Y1, c = 'orange', label = 'False')
    else:
        plt.scatter(X0,Y0,c = 'green', label = T['classification'][0])
        plt.scatter(X1,Y1, c = 'orange', label = T['classification'][1])
    
    plt.xlabel(xlabel = list_columns[i])
    plt.ylabel(ylabel = list_columns[j])
    plt.legend()
    

def plot_data(features,labels,T, max_nb_plot=5): # @Author : CHARREAUX Pierre
    """
    
    IN : 
    ----------
    features : TYPE Pandas dataframe
              DESCRIPTION data which was already cleaned and pre-processed, a line being one data
    
    labels : TYPE numpy array
             DESCRIPTION labels of the data
    
    T : TYPE dictionnary
        DESCRIPTION ( feature name -> (class index -> class name) )

    OUT : 
    -------
    None

    """
    X = features.to_numpy() #data
    y = labels.to_numpy()
    list_columns,numbers_in = give_columns_numbers(features)
    
    nb_data = X.shape[0]
    x_len = X.shape[1] #check the number of coordinates in one data
    nb_plot = 0
    if x_len >= 2:
        for i in range(x_len):
            for j in range(i+1,x_len):
                discret, idx = is_discret(numbers_in,i,j)
                if not discret:
                    plt.subplot(max_nb_plot,max_nb_plot,nb_plot+1)
                    plot_non_discret(nb_data,X,y,T,list_columns,i,j)
                    nb_plot +=1
                
                if nb_plot >= max_nb_plot**2:
                    break
                    break
                
                
                
