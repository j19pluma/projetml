# ML Project

This is our project for the course *Introduction to Machine Learning*.
## Installation

Use the package manager [conda](https://docs.conda.io/en/latest/miniconda.html) to setup the environment.

```bash
git clone https://gitlab.imt-atlantique.fr/j19pluma/projetml
cd projetml
conda env create -f environment.yml
```